#pragma once
#include "Engine/GameObject.h"

//トーラスを管理するクラス
class Torus : public GameObject
{
	int hMode_; //モデルを扱う
public:
	Torus(GameObject* parent);
	~Torus();

	// GameObject を介して継承されました
	//初期化
	//引数 なし
	virtual void Initialize() override;

	//更新
	//引数 なし
	virtual void Update() override;

	//描画
	//引数 なし
	virtual void Draw() override;

	//開放
	//引数 なし
	virtual void Release() override;
};

