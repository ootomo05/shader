#include "TestScene.h"
#include "Engine/SceneManager.h"
#include "Engine/Input.h"
#include "Engine/Image.h"
#include "Engine/Model.h"
#include "Torus.h"

TestScene::TestScene(GameObject * parent)
	: GameObject(parent, "TestScene"),hModel_(-1)
{
}

//初期化
void TestScene::Initialize()
{
	Instantiate<Torus>(this);
}

//更新
void TestScene::Update()
{
}

//描画
void TestScene::Draw()
{
}

//開放
void TestScene::Release()
{
}
