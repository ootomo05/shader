#include "Torus.h"
#include "Engine/Model.h"
#include "Engine/Input.h"

Torus::Torus(GameObject* parent) : GameObject(parent,"Torus"),hMode_ (-1)
{
}

Torus::~Torus()
{
}

void Torus::Initialize()
{
	hMode_ = Model::Load("Assets\\Turas.fbx");
	assert(hMode_ >= 0);
}

void Torus::Update()
{	
	transform_.rotate_.vecY += 0.5f;	
}

void Torus::Draw()
{
	Model::SetTransform(hMode_, transform_);
	Model::Draw(hMode_);
}

void Torus::Release()
{
}
