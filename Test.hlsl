//───────────────────────────────────────
 // テクスチャ＆サンプラーデータのグローバル変数定義
//───────────────────────────────────────
//シェーダ内でテクスチャーを使うための変数
Texture2D	g_texture : register(t0);	//テクスチャー
SamplerState	g_sampler : register(s0);	//サンプラー

//頂点情報以外の情報は
//コンスタントバッファを使ってアクセスする
//───────────────────────────────────────
 // コンスタントバッファ
// DirectX 側から送信されてくる、ポリゴン頂点以外の諸情報の定義
//───────────────────────────────────────
cbuffer global
{
	float4x4	matWVP;			// ワールド・ビュー・プロジェクションの合成行列
	float4x4	matNormal;	    //回転×拡大の逆行列,法線を回転させる変数
	float4x4    matW;           //ワールド行列
	float4      camPos;         //カメラ位置
	float4		diffuseColor;	// ディフューズカラー（マテリアルの色）
	float4      ambientColor;   // アンビエントカラー
	float4      specularColor;  //鏡面反射率
	float       shininess;      //光沢度
	bool		isTexture;		// テクスチャ貼ってあるかどうか
};

//頂点シェーダの出力
struct VS_OUT
{
	float4 pos    : SV_POSITION; //最終的な頂点座標
	float4 normal : TEXCOORD1;   //法線 TEXCOORD0をuv座標として使う
	float4 eye    : TEXCOORD2;   //視点ベクトル
	float2 uv     : TEXCOORD;
};

//頂点シェーダ関数
//引数 pos ローカル座標
//引数 uv  テクスチャのuv座標
//引数 normal 法線
//戻値 頂点座標
//セマンティクス,変数が何かをコンピュータに教える
//戻値 関数名( 引数 : 引数のセマンティクス) : 戻値のセマンティクス
VS_OUT VS(float4 pos : POSITION,float4 uv : TEXCOORD, float4 normal : NORMAL)
{
	VS_OUT outData; //ピクセルシェーダに渡す情報の構造体
	outData.pos =  mul(pos,matWVP); //頂点座標
	outData.uv = uv; //uv座標をピクセルシェーダに渡す

	float4 light = float4(1, -1, 1, 0); //ライト
	light = normalize(light); //内積使って計算するので正規化

	//陰の計算
	//法線の値に変な値が入るので0で初期化
	//法線が物体の回転や拡大に対応するようにする
	//normalとlightの角度を求めて陰を求める
	normal.w = 0; 
	outData.normal = mul(normal, matNormal);

	outData.eye = normalize(camPos - mul(pos, matW)); //視点ベクトル;

	//Phongの反射モデル by wiki
	//ks:鏡面反射係数。入射光に対する鏡面反射率
	//kd:拡散反射係数。入射光に対する拡散反射率
	//ka:環境反射係数。シーン全体を照らす環境光の反射
	//alpha :光沢度
	//Ip = ka * ia + for lights(kd(L * N) * id + ks(R*V)^alpha * is)
	//float4 ambient = float4(0.3, 0.3, 0.3, 1); //ka * ia
	//float4 LN = saturate(dot(normal,-light));  //L * N
	//float4 id = float4(0.5, 0.5, 0.5, 1);      //id
	//float4 diffuse = LN * id;                  //拡散反射光 (L * N) * id
	//float4 R = reflect(light, normal);         //反射ベクトル
	//float4 V = camPos - mul(pos,matW);         //視点ベクトル
	//float ks = 10.f;                            //鏡面反射係数
	//float alpha = 5.0f;                        //光沢度
	//float specular = ks * pow(saturate(dot(R, V)), alpha); //ks * (R・V)^alpha * is (isは省略)

	//outData.color = ambient + diffuse + supecur; //グローシェーディング
	
	return outData; //計算した結果を返す
}

//ピクセルシェーダ関数
//引数 pos
//戻値 ピクセルを光らせる色
float4 PS(VS_OUT inData) : SV_TARGET
{
	float4 light = float4(1, -1, 1, 0); //ライト
	light = normalize(light); //内積使って計算するので正規化

	//法線の補完により長さが変わってしまうので正規化をする
	inData.normal = normalize(inData.normal);
	inData.eye = normalize(inData.eye);

	//Phongの反射モデル by wiki
	//ks:鏡面反射係数。入射光に対する鏡面反射率
	//kd:拡散反射係数。入射光に対する拡散反射率
	//ka:環境反射係数。シーン全体を照らす環境光の反射
	//alpha :光沢度
	//Ip = ka * ia + for lights(kd(L * N) * id + ks(R*V)^alpha * is)
	float4 ambient = ambientColor; //ka * ia
	float4 LN = saturate(dot(-light,inData.normal));  //L * N

	float4 id;
	if (isTexture == true)
	{
		id = g_texture.Sample(g_sampler, inData.uv); //テクスチャからuv座標で指定した色を取得
	}
	else
	{
		id = diffuseColor;                  //id
	}

	float4 diffuse = LN * id;        //拡散反射光 (L * N) * id
	float4 R = reflect(light, inData.normal);  //反射ベクトル
	float4 V = inData.eye;                     //視点ベクトル
	float4 ks = specularColor;                      //鏡面反射係数
	float alpha = shininess;                        //光沢度
	float4 specular = ks * pow(saturate(dot(R, V)), alpha); //ks * (R・V)^alpha * is (isは省略)

	return ambient + diffuse + specular;//最終的な色
}