//インクルード
#include <Windows.h>
#include <limits>
#include <stdlib.h>
#include "Engine/Direct3D.h"
#include "Engine/Input.h"
#include "Engine/RootJob.h"

#pragma comment(lib,"winmm.lib")

//定数宣言
const char* WIN_CLASS_NAME = "SampleGame";  //ウィンドウクラス名
const int WINDOW_WIDTH  = 800;  //ウィンドウの幅
const int WINDOW_HEIGHT = 600; //ウィンドウの高さ

//画面の更新量を固定するか
//本気で更新して更新りょうを調整する

//プロトタイプ宣言
//LRESULT プロシージャの戻り値
//CALLBACK　なんかあったらとりあえず呼ぶ関数
//引数　hwnd ウィンドウ作成時に番号を付与する　ウィンドウの識別番号　何かがあったウィンドウの番号
//引数　msg	何があったか
//引数  wParam  msgの追加情報
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

//ダイアログプロシージャ
//引数 hDlg
//引数 msg
//引数 wp
//引数 lp
BOOL CALLBACK DialogProc(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp);

//ゲームに出てくるすべてのオブジェクトの親
RootJob* pRootJob = nullptr;

//エントリーポイント
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
	//ウィンドウクラス（設計図）を作成
	WNDCLASSEX wc;
	wc.cbSize = sizeof(WNDCLASSEX);                    //この構造体のサイズ
	wc.hInstance = hInstance;                          //インスタンスハンドル
	wc.lpszClassName = WIN_CLASS_NAME;                 //ウィンドウクラス名
	wc.lpfnWndProc = WndProc;                          //ウィンドウプロシージャ
	wc.style = CS_VREDRAW | CS_HREDRAW;                //スタイル（デフォルト）
	wc.hIcon = LoadIcon(nullptr, IDI_APPLICATION);     //アイコン
	wc.hIconSm = LoadIcon(nullptr, IDI_APPLICATION);   //小さいアイコン
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW);       //マウスカーソル
	wc.lpszMenuName = nullptr;                         //メニュー(なし)
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH); //背景（白）
	RegisterClassEx(&wc);  //クラスを登録

	//ウィンドウサイズの計算
	RECT winRect = { 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT };
	AdjustWindowRect(&winRect, WS_OVERLAPPEDWINDOW, TRUE);
	int winW = winRect.right - winRect.left;     //ウィンドウ幅
	int winH = winRect.bottom - winRect.top;     //ウィンドウ高さ

	//ウィンドウを作成
	HWND hWnd = CreateWindow(
		WIN_CLASS_NAME,         //ウィンドウクラス名
		WIN_CLASS_NAME,         //タイトルバーに表示する内容
		WS_OVERLAPPEDWINDOW,    //スタイル（普通のウィンドウ）
		CW_USEDEFAULT,          //表示位置左（おまかせ）
		CW_USEDEFAULT,          //表示位置上（おまかせ）
		winW,                   //ウィンドウ幅
		winH,                   //ウィンドウ高さ
		nullptr,                //親ウインドウ（なし）
		nullptr,                //メニュー（なし）
		hInstance,              //インスタンス
		nullptr                 //パラメータ（なし）
	);
	//ウィンドウを表示
	ShowWindow(hWnd, nCmdShow);

	//~~~~もろもろの初期化処理~~~~~

	//Direct3D初期化
	if (FAILED(Direct3D::Initialize(WINDOW_WIDTH, WINDOW_HEIGHT, hWnd)))
	{
		return 0;
	};

	//DirectInputの初期化
	if (FAILED(Input::Initialize(hWnd)))
	{
		return 0;
	};

	//初期化
	pRootJob = new RootJob;
	pRootJob->Initialize();

	//メッセージループ（何か起きるのを待つ）
	MSG msg;
	ZeroMemory(&msg, sizeof(msg));
	
	
	while (msg.message != WM_QUIT)
	{
		
		//メッセージあり
		if (PeekMessage(&msg, nullptr, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		//メッセージなし
		else
		{
			//windowsの時間関係精度が悪いので
			//時間精度を上げる
			timeBeginPeriod(1);

			//実行速度を制御する
			static DWORD countFps = 0;              //何回処理したか
			static DWORD startTime = timeGetTime(); //osが立ち上がった時の時間を覚えておく
			DWORD nowTime = timeGetTime();          //osが立ち上がってから立った時間
			static DWORD lastUpdateTime = nowTime; 

			//nowTime - startTimeで処理を行った時間
			//1000ミリ秒つまり1秒たったら以下の処理を行う
			if (nowTime - startTime >= 1000)
			{
				//Windowのタイトルにfpsを表示
				char str[16];
				wsprintf(str, "%u", countFps);
				SetWindowText(hWnd, str);

				//1秒間に60フレームの処理を行うので
				//1秒たったら0に戻す
				countFps = 0;

				//処理を行った時間を計測するために
				//計測し始める時間をこのif文を実行している
				//時間に更新
				startTime = nowTime;
			}

			//誤差を減らすために
			//左辺に60をかける
			if ((nowTime - lastUpdateTime) * 60 <= 1000.0f)
			{
				continue;
			}

			lastUpdateTime = nowTime;

			countFps++; //何フレーム目かを計測
			

			
			//ゲームの処理
			
			//入力情報の更新
			Input::Update();

			//すべての子供の更新
			pRootJob->UpdateSub();

			//描画開始
			Direct3D::BeginDraw();

			//これが実行されると
			//RootJobの子供すべての
			//Drawを行う
			pRootJob->DrawSub();

			//描画終了
			Direct3D::EndDraw();
		
			//1ミリ秒休ませる
			Sleep(1);

			//精度を高めるの終わり
			timeEndPeriod(1);
		}
		
	}

	
	//後始末
	pRootJob->ReleaseSub();
	SAFE_DELETE(pRootJob);
	Input::Release();
	Direct3D::Release();

	//プログラム終了
	return 0;
}

//ウィンドウプロシージャ（何かあった時によばれる関数）
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	static int wheelFraction = 0; //回転量の端数
	int zDelta = 0;

	int a = 0;
	switch (msg)
	{
	case WM_MOUSEMOVE: //ウィンドウ上でマウスが動いた
		Input::SetMousePosition(LOWORD(lParam), HIWORD(lParam));
		return 0;
	case WM_DESTROY:
		PostQuitMessage(0);  //プログラム終了
		return 0;
	}

	//いい感じに処理してくれる
	return DefWindowProc(hWnd, msg, wParam, lParam);
}