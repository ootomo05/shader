#pragma once
#include <string>
#include "Transform.h"
#include "Sprite.h"

//画像を管理する
namespace Image
{
	struct ImageData
	{
		std::string fileName; //画像ファイル名
		Transform transform; //変形
		Sprite* pSprite; //画像をポリゴンに張り付ける
	};

	//画像のロード
	//引数 fileName 読み込みたいファイル
	//戻り値 登録場所
	int Lode(std::string fileName);

	//変形させる
	//引数 hImage 変形したい画像のアクセス場所
	//引数 transform 変形
	void SetTransform(int hImage,Transform& transform);

	//描画
	//引数 hImage 描画したい画像の場所
	void Draw(int hImage);

	//確保した素材の開放
	//引数 なし
	void Relese();
};

