#pragma once
#include "Transform.h"

//レイの計算を行う
namespace Math
{
	//行列式
	//引数 a
	//引数 b
	//引数 c
	//戻値 行列式の結果
	float Det(XMVECTOR a, XMVECTOR b, XMVECTOR c);

	//レイと三角形ポリゴンが交差しているか調べる
	//引数 origin レイの発射地点
	//引数 ray レイの発射方向
	//引数 v0 三角形の頂点0
	//引数 v1 三角形の頂点1
	//引数 v2 三角形の頂点2
	//引数 t  レイが三角形に当たった時のレイの発射地点と三角形までの距離
	//戻値 レイと三角形が交差していればtrueを返す
	bool Intersect(XMVECTOR origin, XMVECTOR ray, XMVECTOR v0, XMVECTOR v1, XMVECTOR v2,float* t);
};

