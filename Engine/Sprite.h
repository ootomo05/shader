#pragma once

#include <DirectXMath.h>
#include <string>
#include "Direct3D.h"
#include "Texture.h"
using namespace DirectX;

class Transform;

class Sprite
{

	float imgH_;
	float imgW_;
protected:
	/*
	~~~~継承先だけに見せたい変数や
	関数をの宣言を記述~~~~
	*/

	//コンスタントバッファー
	//2Dで必要なものワールド行列
	struct CONSTANT_BUFFER
	{
		XMMATRIX    matW;//ワールド行列
	};

	//頂点情報
	//頂点情報とuv座標
	struct VERTEX
	{
		XMVECTOR position;//頂点情報
		XMVECTOR uv;//uv座標情報
	};

	

	//1,2,3の順に実態を作るので領域を解放するときは逆の順にする
	ID3D11Buffer *pVertexBuffer_;	//頂点バッファ	1
	ID3D11Buffer *pIndexBuffer_;	//	2
	ID3D11Buffer *pConstantBuffer_;	//コンスタントバッファ 3
	//テクスチャを扱うクラスへアクセスするポインタ
	Texture* pTexture_;
	int indexElementCount_;//index[]の要素数がいくらになるかを入れる

	//頂点バッファの設定
	//引数   なし
	//戻り値 成功か失敗
	HRESULT InitVertex();

	//インデックスバッファの設定
	//引数   なし
	//戻り値 成功か失敗
	HRESULT InitIndexBuffer();

	//コンスタントバッファの設定
	//引数   なし
	//戻り値 成功か失敗
	HRESULT InitConstantBuffer();

	//シェーダに渡す情報をコンスタントバッファに与える
	//引数   worldMatrix　ワールド行列
	//戻り値 なし
	void SetConstantBuffer(Transform& transform);
	//シェーダに渡す
	void PassShade();

public:

	/*
	~~~~全員に見せる変数や関数記述~~~~
	*/
	//コンストラクタ
	Sprite();

	//デストラクタ
	~Sprite();

	//初期化
	//引数	なし
	//戻り値	成功か失敗
	HRESULT Initialize(const std::string imgFileName);


	//描画
	//引数	なし
	//戻り値	なし
	void Draw(Transform& transform);



	//開放
	//引数	なし
	//戻り値	なし
	void AllDelete();

};