#include "Fbx.h"
#include "Direct3D.h"
#include "Camera.h"
#include "Texture.h"
#include "Math.h"

Fbx::Fbx() : vertexCount_(0), polygonCount_(0), pVertexBuffer_(nullptr), pIndexCountEachMaterial_(nullptr),
             ppIndexBuffer_(nullptr), pConstantBuffer_(nullptr), pMaterialList_(nullptr), materialCount_(0),
	         pVertices_(nullptr),ppIndex_(nullptr)
{
}

Fbx::~Fbx()
{
}

//モデルデータの読み込み
HRESULT Fbx::Load(std::string fileName)
{
	//マネージャを生成
	//fbx全体を管理する
	FbxManager* pFbxManager = FbxManager::Create();

	//インポーターを生成
	//fbxファイルを開く
	FbxImporter *fbxImporter = FbxImporter::Create(pFbxManager, "imp");
	bool isFileExist = fbxImporter->Initialize(fileName.c_str(), -1, pFbxManager->GetIOSettings());
	if (!isFileExist)
	{
		MessageBox(nullptr, "fbxファイルが存在しない", "エラー", MB_OK);
		return E_FAIL;
	}
	//シーンオブジェクトにFBXファイルの情報を流し込む
	//Mayaで作ったデータをシーンという
	//モデルをMaya内で作った時のモデルデータ、カメラや光源のデータをfbxファイルから取得
	//fbxImporterはデータを取得したら必要ないので殺す
	FbxScene*	pFbxScene = FbxScene::Create(pFbxManager, "fbxscene");
	fbxImporter->Import(pFbxScene);
	fbxImporter->Destroy();


	//メッシュ情報を取得
	//ルートノードを取得
	//ルートノードの最初の子どものデータを取得
	//頂点情報はメッシュの中に入っているので最初のこどものメッシュを取得
	FbxNode* pRootNode = pFbxScene->GetRootNode();
	FbxNode* pNode = pRootNode->GetChild(0);
	fbxsdk::FbxMesh* pMesh = pNode->GetMesh(); //FbxMeshがあいまいといわれるので、fbxsdkの変数型というの教える

	//各情報の個数を取得
	vertexCount_ = pMesh->GetControlPointsCount();	//頂点の数
	polygonCount_ = pMesh->GetPolygonCount();	    //ポリゴンの数
	materialCount_ = pNode->GetMaterialCount();     //マテリアルの個数
	
	//ディレクトリを変更したのを戻すために
	//現在のカレントディレクトリを取得
	char defaultCurrentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);

	//カレントディレクトリをテクスチャが置いてあるディレクトリに変更するために
	//引数のfileNameからディレクトリ部分を取得
	//_splitpath_sの第一引数がchar型のためstringからchar型に変換する
	char dir[_MAX_DIR]; //ディレクトリ
	_splitpath_s(fileName.c_str() , nullptr, 0, dir, _MAX_DIR, nullptr, 0, nullptr, 0);

	//カレントディレクトリを
	//テクスチャが置いてあるディレクトリに変更
	SetCurrentDirectory(dir);
	
	//頂点バッファ準備
	if (FAILED(InitVertex(pMesh)))
	{
		return E_FAIL;
	};
	//インデックスバッファ準備
	if (FAILED(InitIndex(pMesh)))
	{
		return E_FAIL;
	};
	//コンスタントバッファ準備
	if (FAILED(InitConstantBuffer()))
	{
		return E_FAIL;
	};
	if (FAILED(InitMaterial(pNode)))
	{
		return E_FAIL;
	};

	//カレントディレクトリを元に戻す
	SetCurrentDirectory(defaultCurrentDir);


	//マネージャ解放
	pFbxManager->Destroy();

	//fbxファイルをロードできた
	return S_OK;
}

//マテリアルのデータを調べる
HRESULT Fbx::InitMaterial(fbxsdk::FbxNode* pNode)
{
	//マテリアル数分の情報を記憶するために
	//マテリアル数分の要素数を確保
	pMaterialList_ = new MATERIAL[materialCount_];

	//すべてのマテリアルの情報を取得する
	for (int i = 0; i < materialCount_; i++)
	{
		//i番目のマテリアル情報を取得
		FbxSurfacePhong* pMaterial = (FbxSurfacePhong*)pNode->GetMaterial(i);
		
		//ディフューズカラーを取得する
		FbxDouble3  diffuse = pMaterial->Diffuse;
		pMaterialList_[i].diffuse = XMFLOAT4((float)diffuse[0], (float)diffuse[1], (float)diffuse[2], 1.0f);

		//アンビエントカラーを取得する
		FbxDouble3 ambient = pMaterial->Ambient;
		pMaterialList_[i].ambientColor = XMFLOAT4((float)ambient[0], (float)ambient[1], (float)ambient[2], 1.0f);
		
		pMaterialList_[i].specular = XMFLOAT4(0.0f,0.0f,0.0f,1.0f); //鏡面反射率
		pMaterialList_[i].shininess = 1.0f;  //光沢度

		//マテリアルにフォンシェーダが使われていたら処理する
		if (pMaterial->GetClassId().Is(FbxSurfacePhong::ClassId))
		{
			//鏡面反射率の設定
			FbxDouble3 specular = pMaterial->Specular;
			pMaterialList_[i].specular = XMFLOAT4((float)specular[0], (float)specular[1], (float)specular[2], 1.0f);

			pMaterialList_[i].shininess = (float)pMaterial->Shininess; //光沢度
		}

		//FbxDouble3 specular = pMaterial->Specular;
		//pMaterialList_[i].specular = XMFLOAT4((float)specular[0], (float)specular[1], (float)specular[2], 1.0f);
		//テクスチャ情報
		//マテリアル自身の色情報を取得
		fbxsdk::FbxProperty  lProperty = pMaterial->FindProperty(FbxSurfacePhong::sDiffuse);
		
		//テクスチャの枚数
		//テクスチャが貼られているなら、この変数の値は1以上になる
		//テクスチャが貼られているかの判定に使う
		int fileTextureCount = lProperty.GetSrcObjectCount<fbxsdk::FbxFileTexture>();

		pMaterialList_[i].pTexture = nullptr; //テクスチャがないのであれば作成しない

		//マテリアルごとにテクスチャが貼られているかを調べる
		//テクスチャが貼られているなら、テクスチャの情報を
		//マテリアルから取得する
		//テクスチャあり
		if (fileTextureCount)
		{

			//テクスチャのファイル名を取得
			FbxFileTexture* textureInfo = lProperty.GetSrcObject<FbxFileTexture>(0);
			const char* textureFilePath = textureInfo->GetRelativeFileName();
			
			//絶対パス含まれている場合があるので
			//ファイル名+拡張だけにする
			//ファイルパスの分割。取得する必要がない場所はnullptr
			//ファイル名と拡張子を合体
			char name[_MAX_FNAME];	//ファイル名
			char ext[_MAX_EXT];	//拡張子
			_splitpath_s(textureFilePath, nullptr, 0, nullptr, 0, name, _MAX_FNAME, ext, _MAX_EXT);
			wsprintf(name, "%s%s", name, ext);


			//ファイルからテクスチャ作成
			//テクスチャをロード
			//ちゃんと読み込めたかチェック
			pMaterialList_[i].pTexture = new Texture;
			if (FAILED(pMaterialList_[i].pTexture->Load(name)))
			{
				return E_FAIL;
			};
		}
	}
	
	//マテリアルの情報を全部調べた
	return S_OK;
}

//頂点バッファ準備
HRESULT Fbx::InitVertex(fbxsdk::FbxMesh * mesh)
{
	//頂点個数の配列を作成
	pVertices_ = new VERTEX[vertexCount_];

	//全ポリゴン
	for (int poly = 0; poly < polygonCount_; poly++)
	{
		//3頂点分
		for (int vertex = 0; vertex < 3; vertex++)
		{
			//調べる頂点の番号
			//poly番目の頂点情報を取得
			int index = mesh->GetPolygonVertex(poly, vertex);

			//頂点の位置
			//(float)-pos[0]はMayaとDirectXではZ軸の向きが違うので左右反転しないように"-"をかけている
			FbxVector4 pos = mesh->GetControlPointAt(index);
			pVertices_[index].position = XMVectorSet((float)-pos[0], (float)pos[1], (float)pos[2], 0.0f);


			//頂点の法線
			//陰をつけるために法線情報を取得して
			//頂点情報に加える
			FbxVector4 Normal;
			mesh->GetPolygonVertexNormal(poly, vertex, Normal);	//ｉ番目のポリゴンの、ｊ番目の頂点の法線をゲット
			pVertices_[index].normal = XMVectorSet((float)-Normal[0], (float)Normal[1], (float)Normal[2], 0.0f);

			//頂点のUV
			//UV0番だけ見る
			//Mayaでは画像の座標がY軸だけ変わるので反転させるために
			//FbxVector2 uv = mesh->GetLayer(0)->GetUVs()->GetDirectArray().GetAt(index);
			//pVertices_[index].uv = XMVectorSet((float)uv.mData[0], (float)(1.0 - uv.mData[1]), 0, 0);
			//頂点のUV
			FbxLayerElementUV * pUV = mesh->GetLayer(0)->GetUVs();
			int uvIndex = mesh->GetTextureUVIndex(poly, vertex, FbxLayerElement::eTextureDiffuse);
			FbxVector2  uv = pUV->GetDirectArray().GetAt(uvIndex);
			pVertices_[index].uv = XMVectorSet((float)uv.mData[0], (float)(1.0f - uv.mData[1]), 0.0f, 0.0f);
		}
	}

	// 頂点データ用バッファの設定
	// 一つ一つの頂点が持つ情報をシェーダに渡せるように
	// pVertexBuffer_に設定してやる
	// 最後にpVertexBuffer_がちゃんと作成できたかチェック
	D3D11_BUFFER_DESC bd_vertex;//頂点データ用バッファ用の構造体
	bd_vertex.ByteWidth = sizeof(VERTEX) * vertexCount_;
	bd_vertex.Usage = D3D11_USAGE_DEFAULT;
	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd_vertex.CPUAccessFlags = 0;
	bd_vertex.MiscFlags = 0;
	bd_vertex.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA data_vertex;
	data_vertex.pSysMem = pVertices_;//入れたい情報
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_)))
	{
		MessageBox(nullptr, "Quad.cpp：頂点バッファ作成失敗", "エラー", MB_OK);
		return E_FAIL;          //:pVertexBuffer_作成でなんか問題あるぞ！
	}

	return S_OK;
}

//インデックスバッファ準備
HRESULT Fbx::InitIndex(fbxsdk::FbxMesh * mesh)
{
	//マテリアルごとに情報をわけるので
	//マテリアルの個数分作る
	ppIndexBuffer_ = new ID3D11Buffer*[materialCount_];
	pIndexCountEachMaterial_ = new int[materialCount_];

	//マテリアル個数分の要素数を確保
	ppIndex_ = new int*[materialCount_];
	for (int i = 0; i < materialCount_; i++)
	{
		//頂点数情報を取得するので3をかける
		//とりあえず大きめにとる
		ppIndex_[i] = new int[polygonCount_ * 3];
	}

	//マテリアル事に情報を分割するので
	//マテリアルの個数回繰り返す
	for (int i = 0; i < materialCount_; i++)
	{
		int count = 0;  //インデックス情報が何個あるか

		//全ポリゴンの頂点情報を取得
		for (int poly = 0; poly < polygonCount_; poly++)
		{
			//マテリアルの情報を取得
			//何番目のマテリアルかを取得
			FbxLayerElementMaterial *   mtl = mesh->GetLayer(0)->GetMaterials();
			int mtlId = mtl->GetIndexArray().GetAt(poly);


			//同じ番号だったら
			//頂点情報を取得
			if (mtlId == i)
			{
				//3頂点分の頂点情報を取得
				//反時計回りにする
				for (int vertex = 2; vertex >= 0; vertex--)
				{
					ppIndex_[i][count] = mesh->GetPolygonVertex(poly, vertex);
					count++;
				}
			}
		}

		//i番目のマテリアルが張られている頂点の数
		pIndexCountEachMaterial_[i] = count;
	
		// インデックスバッファを生成する
		//ByteWidthの大きさは一ポリゴンには頂点が3つ、それに4バイトかければ求まる
		D3D11_BUFFER_DESC   bd;//インデックスバッファ用の構造体
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(int) * count;
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA InitData;
		InitData.pSysMem = ppIndex_[i];
		InitData.SysMemPitch = 0;
		InitData.SysMemSlicePitch = 0;
		if (FAILED(Direct3D::pDevice->CreateBuffer(&bd, &InitData, &ppIndexBuffer_[i])))
		{
			MessageBox(nullptr, "Quad.cpp：インデックスバッファ作成失敗", "エラー", MB_OK);
			return E_FAIL;//:ppIndexBuffer_作成でなんか問題あるぞ！
		}
	}

	
	return S_OK;
}


//コンスタントバッファの準備
HRESULT Fbx::InitConstantBuffer()
{
	//コンスタントバッファ作成
	//モデルに付随するカメラやライトの情報を渡す
	D3D11_BUFFER_DESC cb;//コンスタントバッファ用の構造体
	cb.ByteWidth = sizeof(CONSTANT_BUFFER);
	cb.Usage = D3D11_USAGE_DYNAMIC;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cb.MiscFlags = 0;
	cb.StructureByteStride = 0;
	//pConstantBuffer生成
	if (FAILED(Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_)))
	{
		MessageBox(nullptr, "Quad.cpp：コンスタントバッファ作成失敗", "エラー", MB_OK);
		return E_FAIL;//:pConstantBuffer作成でなんか問題あるぞ！
	}

	return S_OK;
};

//描画
void Fbx::Draw(Transform & transform)
{
	//使うシェーダの種類を決める
	Direct3D::SetShaderBundle(SHADER_TEST);

	//頂点バッファ
	UINT stride = sizeof(VERTEX);
	UINT offset = 0;
	Direct3D::pContext->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);

	//コンスタントバッファ
	Direct3D::pContext->VSSetConstantBuffers(0, 1, &pConstantBuffer_);	//頂点シェーダー用	
	Direct3D::pContext->PSSetConstantBuffers(0, 1, &pConstantBuffer_);	//ピクセルシェーダー用


	//頂点情報をマテリアルの個数分に分けたので
	//それぞれ渡してやる。
	for (int i = 0;i < materialCount_;i++)
	{

		// インデックスバッファーをセット
		stride = sizeof(int);
		offset = 0;
		Direct3D::pContext->IASetIndexBuffer(ppIndexBuffer_[i], DXGI_FORMAT_R32_UINT, 0);


		//シェーディングに必要な各情報をシェーダに渡すための準備
		//コンスタントバッファに蓄えて一気にシェーダに渡す
		//XMMatrixTranspose()シェーダ用行列に変換してくれる関数
		CONSTANT_BUFFER cb; // シェーダに渡す情報を入れる変数
		cb.matWVP = XMMatrixTranspose((transform.GetWorldMatrix() * Camera::GetViewMatrix() *
		Camera::GetProjectionMatrix()));                                  //ワールド行列、ビュー行列、プロジェクション行列を合成したもの,各頂点をスクリーン座標に変換するために必要
		cb.matNormal = XMMatrixTranspose(transform.matRotate_ * XMMatrixInverse(nullptr, transform.matScale_));//回転行列,陰の向きを計算するために渡す
		cb.matW = XMMatrixTranspose(transform.GetWorldMatrix());  //ワールド行列
		cb.diffuseColor = pMaterialList_[i].diffuse;     //モデル自身の色,テクスチャが張っていない場合この色が出る
		cb.isTexture = 0;                            //テクスチャが張られていなければfalseを渡す
		cb.SpecularColor = pMaterialList_[i].specular;   //鏡面反射カラー,ハイライトの計算に必要
		cb.shininess = pMaterialList_[i].shininess;      //光沢度,ハイライトの計算に必要
		cb.ambientColor = pMaterialList_[i].ambientColor;//環境光,陰の明るさ
		XMStoreFloat4(&cb.camPos, Camera::position_);    //XMFLOAT4型に変換してcv.camPosに代入する

		//テクスチャとサンプラーをシェーダに渡す処理
		//サンプラーは使いまわす
		ID3D11SamplerState* pSampler = Direct3D::pToonTex->GetSampler();
		Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);

		//シェーダリソースビュー
		//Toon.hlslのg_toonTexにテクスチャーを渡す
		ID3D11ShaderResourceView* pSRV = Direct3D::pToonTex->GetSRV();
		Direct3D::pContext->PSSetShaderResources(1, 1, &pSRV);

		//マテリアルがないときはテクスチャをシェーダに渡す必要がないので処理を飛ばす
		if (pMaterialList_[i].pTexture)
		{
			////テクスチャとサンプラーをシェーダに渡す処理
			//ID3D11SamplerState* pSampler = pMaterialList_[i].pTexture->GetSampler();
			//Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);

			//シェーダリソースビュー
			ID3D11ShaderResourceView* pSRV = pMaterialList_[i].pTexture->GetSRV();
			Direct3D::pContext->PSSetShaderResources(0, 1, &pSRV);

			cb.isTexture = 1; //テクスチャが張られているフラグ
		}

		


		//コンスタントバッファに情報をコピー
		D3D11_MAPPED_SUBRESOURCE pdata;
		Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata);	// GPUからのデータアクセスを止める
		memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));	                // データを送る
		Direct3D::pContext->Unmap(pConstantBuffer_, 0);	//再開

	
		//マテリアルごとに描画する
		Direct3D::pContext->DrawIndexed(pIndexCountEachMaterial_[i], 0, 0);//描画
	}
}

//レイ判定
void Fbx::RayCast(RayCastData* rayData)
{
	//マテリアル毎
	for (DWORD i = 0; i < materialCount_; i++)
	{
		//そのマテリアルのポリゴン毎
		for (DWORD j = 0; j < pIndexCountEachMaterial_[i] / 3; j++)
		{
			//3頂点
			XMVECTOR v0 = pVertices_[ppIndex_[i][j * 3 + 0]].position;
			XMVECTOR v1 = pVertices_[ppIndex_[i][j * 3 + 1]].position;
			XMVECTOR v2 = pVertices_[ppIndex_[i][j * 3 + 2]].position;

			//その三角形との判定
			rayData->hit = Math::Intersect(rayData->start,rayData->dir,v0,v1,v2,&rayData->dist);

			//当たってたら終わり
			if (rayData->hit)
			{
				return;
			}
		}
	}
}

//開放
void Fbx::Release()
{
	SAFE_RELEASE(pConstantBuffer_);
	//マテリアルの個数分newしたのでその分開放
	for (int i = 0; i < materialCount_; i++)
	{
		SAFE_RELEASE(ppIndexBuffer_[i]);
		SAFE_DELETE(ppIndex_[i]);
	}
	SAFE_RELEASE(pVertexBuffer_);
	SAFE_DELETE_ARRAY(ppIndex_);

	SAFE_DELETE_ARRAY(pMaterialList_);
	SAFE_DELETE_ARRAY(pIndexCountEachMaterial_);
	SAFE_DELETE_ARRAY(pVertices_);
}
