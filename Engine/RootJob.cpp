#include "RootJob.h"
#include "SceneManager.h"
//コンストラクタ
RootJob::RootJob()
{
}

//デストラクタ
RootJob::~RootJob()
{
}

//初期化
void RootJob::Initialize()
{
	Instantiate<SceneManager>(this); //シーンマネージャー
}

//更新
void RootJob::Update()
{
}

//描画
void RootJob::Draw()
{

}

//開放
void RootJob::Release()
{

}
