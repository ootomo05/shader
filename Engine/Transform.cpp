#include "Transform.h"


XMMATRIX matTranslate_;	//移動行列
XMMATRIX matRotate_;	//回転行列	
XMMATRIX matScale_;	    //拡大行列

Transform::Transform()
{

	//XMMatrixIdentityは行列を単位行列にしてくれる。
	//scale_を変えるときは元のサイズを1と考えるためサイズベクトルはすべて1で初期化
	matTranslate_ = XMMatrixIdentity();
	matRotate_    = XMMatrixIdentity();
	matScale_     = XMMatrixIdentity();
	position_     = XMVectorSet( 0,0,0,0 );
	rotate_       = XMVectorSet( 0,0,0,0 );
	scale_        = XMVectorSet( 1,1,1,1 );
}

Transform::~Transform()
{
}

//行列の計算
void Transform::Calclation()
{
	//移動行列にセット
	matTranslate_ = XMMatrixTranslation(position_.vecX, position_.vecY, position_.vecZ);
	
	//回転行列をX軸、Y軸、Z軸分それぞれ求めて
	//2DはZ軸を中心に回転するのでZ軸を優勢にして
	//合成回転行列を作る
	XMMATRIX rotateX, rotateY,rotateZ;
	rotateX = XMMatrixRotationX(XMConvertToRadians(rotate_.vecX));
	rotateY = XMMatrixRotationY(XMConvertToRadians(rotate_.vecY));
	rotateZ = XMMatrixRotationZ(XMConvertToRadians(rotate_.vecZ));
	matRotate_ = rotateZ * rotateX * rotateY;                       //Unityが

	//拡大縮小行列にセット
	matScale_ = XMMatrixScaling(scale_.vecX, scale_.vecY, scale_.vecZ);
}

//ワールド行列の取得
XMMATRIX Transform::GetWorldMatrix()
{
	//ワールド行列を作って返す
	//行列の組み合わせ方によって出力結果が変わるので扱いに注意
	//拡大縮小してから回転して移動するほうが汎用的に使える
	XMMATRIX worldMatrix = matScale_ * matRotate_ * matTranslate_;

	if (pParent_ == nullptr)
	{
		return worldMatrix;
	}

	return worldMatrix * pParent_->GetWorldMatrix();
}
