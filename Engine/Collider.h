#pragma once

#include <DirectXMath.h>

using namespace DirectX;

//プロトタイプ宣言
class GameObject;

//当たり判定用の情報
class Collider
{
protected:
	XMVECTOR center_; //オブジェクトから見た球の中心
	GameObject* pGameObject_; //このコライダーを付けたオブジェクトを参照する
	float radius_; //半径
public:

	//コンストラクタ
	//引数 center 球の中心
	//引数 radius 球の半径
	Collider(XMVECTOR center, float radius);

	//デストラクタ
	virtual ~Collider();

	//当たっているか調べる
	//引数 pTarget 相手のコライダー
	//戻り値 当たっていればtrueを返す
	bool IsHit(Collider* pTarget);

	//半径を返す
	//引数 なし
	//戻り値 なし
	float GetRadius();

	//このコライダーを付けたオブジェクトのアドレスをセットする
	//引数 gameObj このコライダーを付けたオブジェクトのアドレス
	void SetGameObject(GameObject* gameObj);
};

