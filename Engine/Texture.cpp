#include <wincodec.h>
#include "Texture.h"
#include "Direct3D.h"

#pragma comment( lib, "WindowsCodecs.lib" )

Texture::Texture() : 
	pSampler_(nullptr), pSRV_(nullptr)
{
}

Texture::~Texture()
{
}

HRESULT Texture::Load(std::string fileName)
{
	//マルチバイト文字列をワイド文字列に変換する処理
	wchar_t wtext[FILENAME_MAX];
	size_t ret;//使わんけど入れるように用意
	mbstowcs_s(&ret, wtext, fileName.c_str(), fileName.length());
	
	CoInitialize(nullptr);//comの初期化

	IWICImagingFactory *pFactory = nullptr;
	IWICBitmapDecoder *pDecoder = nullptr;
	IWICBitmapFrameDecode* pFrame = nullptr;
	IWICFormatConverter* pFormatConverter = nullptr;
	if (FAILED(CoCreateInstance(CLSID_WICImagingFactory, nullptr, CLSCTX_INPROC_SERVER, IID_IWICImagingFactory, reinterpret_cast<void **>(&pFactory))))
	{
		MessageBox(nullptr, "Texture.cpp：function : CoCreateInstance", "エラー", MB_OK);
		return E_FAIL;
	};
	HRESULT hr = pFactory->CreateDecoderFromFilename(wtext, NULL, GENERIC_READ, WICDecodeMetadataCacheOnDemand, &pDecoder);
	if (FAILED(hr))
	{
		MessageBox(nullptr, "Texture.cpp：function : ファイル名がおかしい", "エラー", MB_OK);
		return E_FAIL;
	}
	if (FAILED(pDecoder->GetFrame(0, &pFrame)))
	{
		MessageBox(nullptr, "Texture.cpp：function : GetFrame", "エラー", MB_OK);
		return E_FAIL;
	};
	if (FAILED(pFactory->CreateFormatConverter(&pFormatConverter)))
	{
		MessageBox(nullptr, "Texture.cpp：function : CreateFormatConverter", "エラー", MB_OK);
		return E_FAIL;
	}
	pFormatConverter->Initialize(pFrame, GUID_WICPixelFormat32bppRGBA, WICBitmapDitherTypeNone, NULL, 1.0f, WICBitmapPaletteTypeMedianCut);

	CoUninitialize();//comをもう使わない

	//画像のサイズを調べる	
	pFormatConverter->GetSize(&imgWidth_, &imgHeight_);

	ID3D11Texture2D*	pTexture;
	D3D11_TEXTURE2D_DESC texdec;
	texdec.Width = imgWidth_;
	texdec.Height = imgHeight_;
	texdec.MipLevels = 1;//手前のものは高解像度で描画、奥のものは低解像度
	texdec.ArraySize = 1;
	texdec.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	texdec.SampleDesc.Count = 1;
	texdec.SampleDesc.Quality = 0;
	texdec.Usage = D3D11_USAGE_DYNAMIC;
	texdec.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	texdec.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	texdec.MiscFlags = 0;
	if (FAILED(Direct3D::pDevice->CreateTexture2D(&texdec, nullptr, &pTexture)))
	{
		MessageBox(nullptr, "Texture.cpp：テクスチャ作成失敗", "エラー", MB_OK);
		return E_FAIL;
	};

	//テクスチャをコンテキストに渡す
	D3D11_MAPPED_SUBRESOURCE hMappedres;
	Direct3D::pContext->Map(pTexture, 0, D3D11_MAP_WRITE_DISCARD, 0, &hMappedres);
	//第二引数 何バイト使うか
	pFormatConverter->CopyPixels(nullptr, imgWidth_ * 4, imgWidth_ * imgHeight_ * 4, (BYTE*)hMappedres.pData);
	Direct3D::pContext->Unmap(pTexture, 0);

	//サンプラー作成
	//テクスチャの張り方を設定
	D3D11_SAMPLER_DESC  SamDesc;
	ZeroMemory(&SamDesc, sizeof(D3D11_SAMPLER_DESC));
	//D3D11_FILTER_MIN_MAG_MIP_LINEAR ぼかす
	//D3D11_FILTER_MIN_MAG_MIP_POINT  ぼかさない
	//D3D11_TEXTURE_ADDRESS_WRAP テクスチャをループさせる
	SamDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT; //アンチエイリアシングの設定
	SamDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;  //テクスチャをループさせるか
	SamDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	SamDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	if (FAILED(Direct3D::pDevice->CreateSamplerState(&SamDesc, &pSampler_)))
	{
		MessageBox(nullptr, "Texture.cpp：サンプラー作成失敗", "エラー", MB_OK);
		return E_FAIL;
	};

	//シェーダリソースビュー作成
	D3D11_SHADER_RESOURCE_VIEW_DESC srv = {};
	srv.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	srv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srv.Texture2D.MipLevels = 1;
	if (FAILED(Direct3D::pDevice->CreateShaderResourceView(pTexture, &srv, &pSRV_)))
	{
		MessageBox(nullptr, "Texture.cpp：シェーダリソースビュー作成失敗", "エラー", MB_OK);
		return E_FAIL;
	};


	//もう使わない
	SAFE_RELEASE(pTexture);

	return S_OK;
}


void Texture::Release()
{
	//開放
	SAFE_RELEASE(pSRV_);
	SAFE_RELEASE(pSampler_);
}

//サンプラーを見せる
ID3D11SamplerState * Texture::GetSampler()
{
	return pSampler_;
}

//シェーダリソースビューを見せる
ID3D11ShaderResourceView * Texture::GetSRV()
{
	return pSRV_;
}

//画像の横幅
UINT Texture::GetImagWidth()
{
	return imgWidth_;
};

//画像の縦幅
UINT Texture::GetImgHeight()
{
	return imgHeight_;
}