#include "SphereCollider.h"
#include "GameObject.h"

//コンストラクタ
SphereCollider::SphereCollider(DirectX::XMVECTOR pos, float radius) : 
	position_(pos),radius_(radius),pGameObject_(nullptr)
{
}

//デストラクタ
SphereCollider::~SphereCollider()
{
}

//座標を返す
DirectX::XMVECTOR SphereCollider::GetPosition()
{
	return position_;
}

//半径をを返す
float SphereCollider::GetRadius()
{
	return radius_;
}

//ゲームオブジェクトを返す
GameObject * SphereCollider::GetGameObject()
{
	return pGameObject_;
}

//ゲームオブジェクトの登録
void SphereCollider::SetGameObject(GameObject * gameObject)
{
	pGameObject_ = gameObject;
}
