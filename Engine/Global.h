#pragma once

//deleteの代わりに使う
//中身がnllptrのポインタを開放しないように
//安全にdeleteするマクロ
#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}
#define SAFE_RELEASE(p) if(p != nullptr){p->Release();p = nullptr;}
#define SAFE_DELETE_ARRAY(p) if (p != nullptr){delete[] p;p = nullptr;}