#pragma once
#include <vector>
#include <DirectXMath.h>
#include <d3d11.h>
//球体コライダー

class GameObject;

//球体コライダーの情報を管理
class SphereCollider
{
	DirectX::XMVECTOR position_; //コライダーをつける位置
	float radius_; //球体の半径
	GameObject* pGameObject_; //コライダーを使っているオブジェクト
public:

	//コンストラクタ
	//引数 pos コライダーをつける位置
	//引数 radius 球体の半径
	SphereCollider(DirectX::XMVECTOR pos, float radius);

	//デストラクタ
	~SphereCollider();

	//コライダーがつけられている場所を返す
	//引数 なし
	//戻り値 コライダーを設置している座標
	DirectX::XMVECTOR GetPosition();

	//半径を返す
	//引数 なし
	//戻り値 球体の半径
	float GetRadius();

	//コライダーをつけているオブジェクトを返す
	//引数 なし
	//戻り値 コライダーをつけているオブジェクト
	GameObject* GetGameObject();

	//コライダーをつけたオブジェクトの登録
	//引数 gameObject コライダーをつけるオブジェクト
	void SetGameObject(GameObject* gameObject);
};

