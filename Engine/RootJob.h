#pragma once

#include "GameObject.h"

class RootJob : public GameObject
{
	// GameObject を介して継承されました
	
public:
	//コンストラクタ
	RootJob();

	//デストラクタ
	~RootJob();

	//初期化
	//引数 なし
	void Initialize() override;

	//更新
	//引数 なし
	void Update() override;

	//描画
	//引数 なし
	void Draw() override;

	//開放
	//引数 なし
	void Release() override;
};

