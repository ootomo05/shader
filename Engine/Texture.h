#pragma once
#include <d3d11.h>
#include "string"
#include "Global.h"

class Texture
{
	ID3D11SamplerState*	pSampler_;
	ID3D11ShaderResourceView*	pSRV_;

	UINT imgWidth_;//画像の幅
	UINT imgHeight_;//画像の高さ
public:
	Texture();
	~Texture();

	//テクスチャの作成
	//引数	 fileName 画像ファイル名
	//戻り値 成功か失敗
	HRESULT Load(std::string fileName);

	//開放
	//引数	 なし
	//戻り値 なし
	void Release();
	
	//サンプラーを見せる
	//引数	 なし
	//戻り値 pSampler_
	ID3D11SamplerState* GetSampler();

	//シェーダリソースビューを見せる
	//引数	 なし
	//戻り値 pSRV_
	ID3D11ShaderResourceView* GetSRV();

	//画像の縦の幅を返す
	//引数   なし
	//戻り値 imgWidth
	UINT GetImagWidth();

	//画像の縦の幅を返す
	//引数   なし
	//戻り値 imgHeight_
	UINT GetImgHeight();


};