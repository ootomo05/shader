#include <d3dcompiler.h>
#include "Direct3D.h"
#include  "Camera.h"
#include "Texture.h"

//変数
namespace Direct3D
{
	//使用するグラボへのアクセスポインタ
	ID3D11Device*           pDevice = nullptr;		    //デバイス
	//描画処理を行ってくれるクラス
	ID3D11DeviceContext*    pContext = nullptr;		    //デバイスコンテキスト
	//裏と表を切り替える
	IDXGISwapChain*         pSwapChain = nullptr;		    //スワップチェイン
	//描画先にアクセスするポインタ
	ID3D11RenderTargetView* pRenderTargetView = nullptr;	//レンダーターゲットビュー
	ID3D11Texture2D*	pDepthStencil = nullptr;			//深度ステンシル
	ID3D11DepthStencilView* pDepthStencilView = nullptr;		//深度ステンシルビュー

	UINT winWidth;
	UINT winHeight;

	Texture* pToonTex = nullptr;
	//シェーダに渡す情報をまとめた構造多
	struct ShaderBundle
	{
		ID3D11VertexShader*	pVertexShader = nullptr;	    //頂点シェーダー
		ID3D11PixelShader*	pPixelShader = nullptr;		    //ピクセルシェーダー
		ID3D11RasterizerState*	pRasterizerState = nullptr;	//ラスタライザー
		ID3D11InputLayout*	pVertexLayout = nullptr;	    //頂点インプットレイアウト
	};

	ShaderBundle shaderBundle[SHADER_MAX];

}

//初期化
HRESULT Direct3D::Initialize(int winW, int winH, HWND hWnd)
{
	///////////////////////////いろいろ準備するための設定///////////////////////////////
	//いろいろな設定項目をまとめた構造体
	DXGI_SWAP_CHAIN_DESC scDesc;
	
	//とりあえず全部0
	ZeroMemory(&scDesc, sizeof(scDesc));
	
	winHeight = winH;
	winWidth = winW;

	//描画先のフォーマット
	scDesc.BufferDesc.Width = winW;		//画面幅
	scDesc.BufferDesc.Height = winH;	//画面高さ
	scDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;	// 何色使えるか

	//FPS（1/60秒に1回）
	scDesc.BufferDesc.RefreshRate.Numerator = 60;
	scDesc.BufferDesc.RefreshRate.Denominator = 1;

	//その他
	scDesc.Windowed = TRUE;			//ウィンドウモードかフルスクリーンか
	scDesc.OutputWindow = hWnd;		//ウィンドウハンドル
	scDesc.BufferCount = 1;			//バックバッファの枚数
	scDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;	//バックバッファの使い道＝画面に描画するために
	scDesc.SampleDesc.Count = 1;		//MSAA（アンチエイリアス）の設定
	scDesc.SampleDesc.Quality = 0;		//　〃

		////////////////上記設定をもとにデバイス、コンテキスト、スワップチェインを作成////////////////////////
	D3D_FEATURE_LEVEL level;
	if (FAILED(D3D11CreateDeviceAndSwapChain(
		nullptr,				        // どのビデオアダプタを使用するか？既定ならばnullptrで
		D3D_DRIVER_TYPE_HARDWARE,		// ドライバのタイプを渡す。ふつうはHARDWARE
		nullptr,				        // 上記をD3D_DRIVER_TYPE_SOFTWAREに設定しないかぎりnullptr
		0,					            // 何らかのフラグを指定する。（デバッグ時はD3D11_CREATE_DEVICE_DEBUG？）
		nullptr,				        // デバイス、コンテキストのレベルを設定。nullptrにしとけばOK
		0,					            // 上の引数でレベルを何個指定したか
		D3D11_SDK_VERSION,			    // SDKのバージョン。必ずこの値
		&scDesc,				        // 上でいろいろ設定した構造体
		&pSwapChain,				    // 無事完成したSwapChainのアドレスが返ってくる
		&pDevice,				        // 無事完成したDeviceアドレスが返ってくる
		&level,					        // 無事完成したDevice、Contextのレベルが返ってくる
		&pContext)))			        // 無事完成したContextのアドレスが返ってくる
	{
		MessageBox(nullptr, "デバイス、コンテキスト、スワップチェインのどれかの作成が失敗","エラー",MB_OK);
		return E_FAIL;

	}

	///////////////////////////レンダーターゲットビュー作成///////////////////////////////
	//スワップチェーンからバックバッファを取得（バックバッファ ＝ レンダーターゲット）
	ID3D11Texture2D* pBackBuffer;
	pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);

	//レンダーターゲットビューを作成
	if (FAILED(pDevice->CreateRenderTargetView(pBackBuffer, nullptr, &pRenderTargetView)))			// 無事完成したContextのアドレスが返ってくる
	{
		MessageBox(nullptr, "レンダーターゲットビュー作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	//一時的にバックバッファを取得しただけなので解放
	pBackBuffer->Release();

	///////////////////////////ビューポート（描画範囲）設定///////////////////////////////
	//レンダリング結果を表示する範囲
	D3D11_VIEWPORT vp;
	vp.Width = (float)winW;	 //幅
	vp.Height = (float)winH; //高さ
	vp.MinDepth = 0.0f;	     //手前
	vp.MaxDepth = 1.0f;   	 //奥
	vp.TopLeftX = 0;	     //左
	vp.TopLeftY = 0;	     //上


	//深度ステンシルビューの作成
	//zバッファ法を使うための準備
	D3D11_TEXTURE2D_DESC descDepth;
	descDepth.Width = winW;
	descDepth.Height = winH;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_D32_FLOAT;
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;
	pDevice->CreateTexture2D(&descDepth, NULL, &pDepthStencil);
	pDevice->CreateDepthStencilView(pDepthStencil, NULL, &pDepthStencilView);


	//データを画面に描画するための一通りの設定（パイプライン）
	pContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);  // データの入力種類を指定
	pContext->OMSetRenderTargets(1, &pRenderTargetView, pDepthStencilView);             // 描画先を設定
	pContext->RSSetViewports(1, &vp);

	//シェーダー準備
	//準備失敗
	if (FAILED(InitShader()))
	{
	return E_FAIL;
	};


	//カメラの初期化
	Camera::Initialize();

	//テクスチャを読み込む
	pToonTex = new Texture();
	pToonTex->Load("Assets\\Toon.png");

	return S_OK;
}

	//シェーダ準備
HRESULT Direct3D::InitShader()
{
	//3Dシェーダの作成と成功かどうかのチェック
	if (FAILED(InitShader3D()))
	{
		return E_FAIL;
	};

	//2Dシェーダの作成と成功かどうかのチェック
	if (FAILED(InitShader2D()))
	{
		return E_FAIL;
	}

	//テスト用シェーダを作る
	if (FAILED(InitShaderTest()))
	{
		return E_FAIL;
	}

	//トゥーンシェーダ作成
	if (FAILED(InitShaderToon()))
	{
		return E_FAIL;
	}

	//すべてのシェーダの準備が完了
	return S_OK;
}

//使うシェーダの種類を設定
void Direct3D::SetShaderBundle(SHADER_TYPE type)
{
	//それぞれをデバイスコンテキストにセット
	pContext->VSSetShader(shaderBundle[type].pVertexShader, nullptr, 0);	//頂点シェーダー
	pContext->PSSetShader(shaderBundle[type].pPixelShader, nullptr, 0);     //ピクセルシェーダー
	pContext->IASetInputLayout(shaderBundle[type].pVertexLayout);	        //頂点インプットレイアウト
	pContext->RSSetState(shaderBundle[type].pRasterizerState);		        //ラスタライザー
}


	//3Dシェーダの準備
HRESULT Direct3D::InitShader3D()
{
	//~~~~3Dシェーダの作成~~~~
	// 頂点シェーダの作成（コンパイル）
	ID3DBlob *pCompileVS = nullptr;//コンパイルしたシェーダを入れる
	if (FAILED(D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "VS",
		"vs_5_0", 0, 0, &pCompileVS, nullptr)))//コンパイルしたものを用意した変数に入れる
	{
		MessageBox(nullptr, "Direct3D.cpp：シェーダファイルのコンパイル失敗", "エラー", MB_OK);
		return E_FAIL;
	}


	if (FAILED(pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(),
		pCompileVS->GetBufferSize(), nullptr, &shaderBundle[SHADER_3D].pVertexShader)))			// 無事完成したContextのアドレスが返ってくる
	{
		MessageBox(nullptr, "Direct3D.cpp：頂点シェーダ作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}
	// ピクセルシェーダの作成（コンパイル）
	ID3DBlob *pCompilePS = nullptr;//コンパイルしたものを一時的に入れる
	if (FAILED(D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "PS", "ps_5_0", 0, 0, &pCompilePS, nullptr)))
	{
		MessageBox(nullptr, "Direct3D.cpp： function ： D3DCompileFromFile", "エラー", MB_OK);
		return E_FAIL;
	};
	if (FAILED(pDevice->CreatePixelShader(pCompilePS->GetBufferPointer(),
		pCompilePS->GetBufferSize(), nullptr, &shaderBundle[SHADER_3D].pPixelShader)))			// 無事完成したContextのアドレスが返ってくる
	{
		MessageBox(nullptr, "Direct3D.cpp：ピクセルシェーダ作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}
	//頂点インプットレイアウト
	//一つの頂点に持たせる情報を設定
	//後から要素数を増やす
	//POSITION　頂点の座標
	//TEXCOORD	テクスチャをどういう風にはるか
	D3D11_INPUT_ELEMENT_DESC layout[] = {
		//一個目の要素:なんの情報か
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },	//位置
		//五つ目の要素：連続した情報を何倍と目から見ればいいか
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, sizeof(XMVECTOR) , D3D11_INPUT_PER_VERTEX_DATA, 0 },//UV座標
		//法線情報
		{ "NORMAL",	0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof(XMVECTOR) * 2 ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },//法線
	};

	//CreateInputLayoutの第二引数用
	//layoutの要素数を計算
	int sizeLayout = sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC);

	if (FAILED(pDevice->CreateInputLayout(layout, sizeLayout,
		pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &shaderBundle[SHADER_3D].pVertexLayout)))			// 無事完成したContextのアドレスが返ってくる
	{
		MessageBox(nullptr, "Direct3D.cpp：頂点インプットレイアウト", "エラー", MB_OK);
		return E_FAIL;
	}

	//いらなくなったので開放処理
	SAFE_RELEASE(pCompilePS);
	SAFE_RELEASE(pCompileVS);
	
	//ラスタライザ作成
	D3D11_RASTERIZER_DESC rdc = {};
	rdc.CullMode = D3D11_CULL_BACK;//Cullingmodeの略	いらないものを捨ててくれるD3D11_CULL_BACK裏面を消す
	/*
		D3D11_CULL_NONE	設定しない
		D3D11_CULL_FRONT	//ポリゴンの表を描画しない
		D3D11_CULL_BACK	ポリゴンの後ろを描画しない
	*/
	rdc.FillMode = D3D11_FILL_SOLID;//塗りつぶしモード
	/*
		D3D11_FILL_WIREFRAME	枠だけ塗りつぶす
		D3D11_FILL_SOLID	個体を塗りつぶす
	*/
	rdc.FrontCounterClockwise = false;//時計回りに情報を入れるための設定
	//作ったやつをpRasterizerStateに入れる
	if (FAILED(pDevice->CreateRasterizerState(&rdc, &shaderBundle[SHADER_3D].pRasterizerState)))
	{
		return E_FAIL;
	};


	//問題なく3Dシェーダの準備が完了
	return S_OK;
}

	//2Dシェーダの準備
HRESULT Direct3D::InitShader2D()
{
	//~~~~3Dシェーダの作成~~~~
	// 頂点シェーダの作成（コンパイル）
	ID3DBlob *pCompileVS = nullptr;//コンパイルしたシェーダを入れる
	if (FAILED(D3DCompileFromFile(L"Simple2D.hlsl", nullptr, nullptr, "VS",
		"vs_5_0", 0, 0, &pCompileVS, nullptr)))//コンパイルしたものを用意した変数に入れる
	{
		MessageBox(nullptr, "Direct3D.cpp：シェーダファイルのコンパイル失敗", "エラー", MB_OK);
		return E_FAIL;
	}


	if (FAILED(pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(),
		pCompileVS->GetBufferSize(), nullptr, &shaderBundle[SHADER_2D].pVertexShader)))			// 無事完成したContextのアドレスが返ってくる
	{
		MessageBox(nullptr, "Direct3D.cpp：頂点シェーダ作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}
	// ピクセルシェーダの作成（コンパイル）
	ID3DBlob *pCompilePS = nullptr;//コンパイルしたものを一時的に入れる
	if (FAILED(D3DCompileFromFile(L"Simple2D.hlsl", nullptr, nullptr, "PS", "ps_5_0", 0, 0, &pCompilePS, nullptr)))
	{
		MessageBox(nullptr, "Direct3D.cpp： function ： D3DCompileFromFile", "エラー", MB_OK);
		return E_FAIL;
	};
	if (FAILED(pDevice->CreatePixelShader(pCompilePS->GetBufferPointer(),
		pCompilePS->GetBufferSize(), nullptr, &shaderBundle[SHADER_2D].pPixelShader)))			// 無事完成したContextのアドレスが返ってくる
	{
		MessageBox(nullptr, "Direct3D.cpp：ピクセルシェーダ作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}
	//頂点インプットレイアウト
	//一つの頂点に持たせる情報を設定
	//後から要素数を増やす
	//POSITION　頂点の座標
	//TEXCOORD	テクスチャをどういう風にはるか
	D3D11_INPUT_ELEMENT_DESC layout[] = {
		//一個目の要素:なんの情報か
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },	//位置
		//五つ目の要素：連続した情報を何倍と目から見ればいいか
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, sizeof(XMVECTOR) , D3D11_INPUT_PER_VERTEX_DATA, 0 },//UV座標
	};

	//CreateInputLayoutの第二引数用
	//layoutの要素数を計算
	int sizeLayout = sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC);

	if (FAILED(pDevice->CreateInputLayout(layout, sizeLayout,
		pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &shaderBundle[SHADER_2D].pVertexLayout)))			// 無事完成したContextのアドレスが返ってくる
	{
		MessageBox(nullptr, "Direct3D.cpp：頂点インプットレイアウト", "エラー", MB_OK);
		return E_FAIL;
	}

	//いらなくなったので開放処理
	SAFE_RELEASE(pCompilePS);
	SAFE_RELEASE(pCompileVS);


	//ラスタライザ作成
	D3D11_RASTERIZER_DESC rdc = {};
	rdc.CullMode = D3D11_CULL_BACK;//Cullingmodeの略	いらないものを捨ててくれるD3D11_CULL_BACK裏面を消す
	/*
		D3D11_CULL_NONE	設定しない
		D3D11_CULL_FRONT	//ポリゴンの表を描画しない
		D3D11_CULL_BACK	ポリゴンの後ろを描画しない
	*/
	rdc.FillMode = D3D11_FILL_SOLID;//塗りつぶしモード
	/*
		D3D11_FILL_WIREFRAME	枠だけ塗りつぶす
		D3D11_FILL_SOLID	    個体を塗りつぶす
	*/
	rdc.FrontCounterClockwise = false;//時計回りに情報を入れるための設定
	//作ったやつをpRasterizerStateに入れる
	if (FAILED(pDevice->CreateRasterizerState(&rdc, &shaderBundle[SHADER_2D].pRasterizerState)))
	{
		return E_FAIL;
	};

	
		return S_OK;
}

HRESULT Direct3D::InitShaderTest()
{
	//~~~~3Dシェーダの作成~~~~
	// 頂点シェーダの作成（コンパイル）
	ID3DBlob *pCompileVS = nullptr;//コンパイルしたシェーダを入れる
	if (FAILED(D3DCompileFromFile(L"Test.hlsl", nullptr, nullptr, "VS",
		"vs_5_0", 0, 0, &pCompileVS, nullptr)))//コンパイルしたものを用意した変数に入れる
	{
		MessageBox(nullptr, "Direct3D.cpp：シェーダファイルのコンパイル失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	if (FAILED(pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(),
		pCompileVS->GetBufferSize(), nullptr, &shaderBundle[SHADER_TEST].pVertexShader)))			// 無事完成したContextのアドレスが返ってくる
	{
		MessageBox(nullptr, "Direct3D.cpp：頂点シェーダ作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}
	// ピクセルシェーダの作成（コンパイル）
	ID3DBlob *pCompilePS = nullptr;//コンパイルしたものを一時的に入れる
	if (FAILED(D3DCompileFromFile(L"Test.hlsl", nullptr, nullptr, "PS", "ps_5_0", 0, 0, &pCompilePS, nullptr)))
	{
		MessageBox(nullptr, "Direct3D.cpp： function ： D3DCompileFromFile", "エラー", MB_OK);
		return E_FAIL;
	};
	if (FAILED(pDevice->CreatePixelShader(pCompilePS->GetBufferPointer(),
		pCompilePS->GetBufferSize(), nullptr, &shaderBundle[SHADER_TEST].pPixelShader)))			// 無事完成したContextのアドレスが返ってくる
	{
		MessageBox(nullptr, "Direct3D.cpp：ピクセルシェーダ作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}
	//頂点インプットレイアウト
	//一つの頂点に持たせる情報を設定
	//後から要素数を増やす
	//POSITION　頂点の座標
	//TEXCOORD	テクスチャをどういう風にはるか
	D3D11_INPUT_ELEMENT_DESC layout[] = {
		//一個目の要素:なんの情報か
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },	//位置
		//五つ目の要素：連続した情報を何倍と目から見ればいいか
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, sizeof(XMVECTOR) , D3D11_INPUT_PER_VERTEX_DATA, 0 },//UV座標
		//法線情報
		{ "NORMAL",	0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof(XMVECTOR) * 2 ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },//法線
	};

	//CreateInputLayoutの第二引数用
	//layoutの要素数を計算
	int sizeLayout = sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC);

	if (FAILED(pDevice->CreateInputLayout(layout, sizeLayout,
			pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &shaderBundle[SHADER_TEST].pVertexLayout)))			// 無事完成したContextのアドレスが返ってくる
	{
		MessageBox(nullptr, "Direct3D.cpp：頂点インプットレイアウト", "エラー", MB_OK);
		return E_FAIL;
	}

	//いらなくなったので開放処理
	SAFE_RELEASE(pCompilePS);
	SAFE_RELEASE(pCompileVS);

	//ラスタライザ作成
	D3D11_RASTERIZER_DESC rdc = {};
	rdc.CullMode = D3D11_CULL_BACK;//Cullingmodeの略	いらないものを捨ててくれるD3D11_CULL_BACK裏面を消す
	/*
		D3D11_CULL_NONE	設定しない
		D3D11_CULL_FRONT	//ポリゴンの表を描画しない
		D3D11_CULL_BACK	ポリゴンの後ろを描画しない
	*/
	rdc.FillMode = D3D11_FILL_SOLID;//塗りつぶしモード
	/*
		D3D11_FILL_WIREFRAME	枠だけ塗りつぶす
		D3D11_FILL_SOLID	個体を塗りつぶす
	*/
	rdc.FrontCounterClockwise = false;//時計回りに情報を入れるための設定
	//作ったやつをpRasterizerStateに入れる
	if (FAILED(pDevice->CreateRasterizerState(&rdc, &shaderBundle[SHADER_TEST].pRasterizerState)))
	{
		return E_FAIL;
	};

	//問題なく3Dシェーダの準備が完了
	return S_OK;
}

HRESULT Direct3D::InitShaderToon()
{
	//~~~~3Dシェーダの作成~~~~
	// 頂点シェーダの作成（コンパイル）
	ID3DBlob *pCompileVS = nullptr;//コンパイルしたシェーダを入れる
	if (FAILED(D3DCompileFromFile(L"Toon.hlsl", nullptr, nullptr, "VS",
		"vs_5_0", 0, 0, &pCompileVS, nullptr)))//コンパイルしたものを用意した変数に入れる
	{
		MessageBox(nullptr, "Direct3D.cpp：シェーダファイルのコンパイル失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	if (FAILED(pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(),
		pCompileVS->GetBufferSize(), nullptr, &shaderBundle[SHADER_TOON].pVertexShader)))			// 無事完成したContextのアドレスが返ってくる
	{
		MessageBox(nullptr, "Direct3D.cpp：頂点シェーダ作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}
	// ピクセルシェーダの作成（コンパイル）
	ID3DBlob *pCompilePS = nullptr;//コンパイルしたものを一時的に入れる
	if (FAILED(D3DCompileFromFile(L"Toon.hlsl", nullptr, nullptr, "PS", "ps_5_0", 0, 0, &pCompilePS, nullptr)))
	{
		MessageBox(nullptr, "Direct3D.cpp： function ： D3DCompileFromFile", "エラー", MB_OK);
		return E_FAIL;
	};
	if (FAILED(pDevice->CreatePixelShader(pCompilePS->GetBufferPointer(),
		pCompilePS->GetBufferSize(), nullptr, &shaderBundle[SHADER_TOON].pPixelShader)))			// 無事完成したContextのアドレスが返ってくる
	{
		MessageBox(nullptr, "Direct3D.cpp：ピクセルシェーダ作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}
	//頂点インプットレイアウト
	//一つの頂点に持たせる情報を設定
	//後から要素数を増やす
	//POSITION　頂点の座標
	//TEXCOORD	テクスチャをどういう風にはるか
	D3D11_INPUT_ELEMENT_DESC layout[] = {
		//一個目の要素:なんの情報か
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },	//位置
		//五つ目の要素：連続した情報を何倍と目から見ればいいか
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, sizeof(XMVECTOR) , D3D11_INPUT_PER_VERTEX_DATA, 0 },//UV座標
		//法線情報
		{ "NORMAL",	0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof(XMVECTOR) * 2 ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },//法線
	};

	//CreateInputLayoutの第二引数用
	//layoutの要素数を計算
	int sizeLayout = sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC);

	if (FAILED(pDevice->CreateInputLayout(layout, sizeLayout,
		pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &shaderBundle[SHADER_TOON].pVertexLayout)))			// 無事完成したContextのアドレスが返ってくる
	{
		MessageBox(nullptr, "Direct3D.cpp：頂点インプットレイアウト", "エラー", MB_OK);
		return E_FAIL;
	}

	//いらなくなったので開放処理
	SAFE_RELEASE(pCompilePS);
	SAFE_RELEASE(pCompileVS);

	//ラスタライザ作成
	D3D11_RASTERIZER_DESC rdc = {};
	rdc.CullMode = D3D11_CULL_BACK;//Cullingmodeの略	いらないものを捨ててくれるD3D11_CULL_BACK裏面を消す
	/*
		D3D11_CULL_NONE	設定しない
		D3D11_CULL_FRONT	//ポリゴンの表を描画しない
		D3D11_CULL_BACK	ポリゴンの後ろを描画しない
	*/
	rdc.FillMode = D3D11_FILL_SOLID;//塗りつぶしモード
	/*
		D3D11_FILL_WIREFRAME	枠だけ塗りつぶす
		D3D11_FILL_SOLID	個体を塗りつぶす
	*/
	rdc.FrontCounterClockwise = false;//時計回りに情報を入れるための設定
	//作ったやつをpRasterizerStateに入れる
	if (FAILED(pDevice->CreateRasterizerState(&rdc, &shaderBundle[SHADER_TOON].pRasterizerState)))
	{
		return E_FAIL;
	};

	//問題なく3Dシェーダの準備が完了
	return S_OK;
}


//描画開始
void Direct3D::BeginDraw()
{
	//背景の色
	float clearColor[4] = { 0.0f, 0.5f, 0.5f, 1.0f };//R,G,B,Aそれぞれ0.0~1.0までの値で指定
	//画面をクリア
	pContext->ClearRenderTargetView(pRenderTargetView, clearColor);

	//カメラの更新
	Camera::Update();

	//毎フレームカメラからの距離を最大にするために
	//深度バッファクリア
	pContext->ClearDepthStencilView(pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
}

//描画終了
void Direct3D::EndDraw()
{
	//スワップ（バックバッファを表に表示する）
	pSwapChain->Present(0, 0);
}


//解放処理
void Direct3D::Release()
{
	/*
	開放する際は作成した順番の逆から行う
	*/



	//開放処理
	for (int i = 0; i < SHADER_MAX; i++)
	{
		SAFE_RELEASE(shaderBundle[i].pRasterizerState);
		SAFE_RELEASE(shaderBundle[i].pVertexLayout);
		SAFE_RELEASE(shaderBundle[i].pPixelShader);
		SAFE_RELEASE(shaderBundle[i].pVertexShader);
	}
	

	//解放処理
	SAFE_RELEASE(pDepthStencil);			//深度ステンシル
	SAFE_RELEASE(pDepthStencilView);		//深度ステンシルビュー
	SAFE_RELEASE(pRenderTargetView);
	SAFE_RELEASE(pSwapChain);
	SAFE_RELEASE(pContext);
	SAFE_RELEASE(pDevice);
	SAFE_RELEASE(pToonTex);
}

