#pragma once

#include <DirectXMath.h>

using namespace DirectX;

//-----------------------------------------------------------
//カメラ
//-----------------------------------------------------------
namespace Camera
{
	extern XMVECTOR position_;	//カメラの位置（視点）

	//初期化（プロジェクション行列作成）
	//引数	なし
	//戻り値	なし
	void Initialize();

	//更新（ビュー行列作成）
	//引数	なし
	//戻り値	なし
	void Update();

	//視点（カメラの位置）を設定
	//引数	position カメラの位置
	//戻り値	なし
	void SetPosition(XMVECTOR position);

	//焦点（見る位置）を設定
	//引数	target 焦点
	//戻り値	なし
	void SetTarget(XMVECTOR target);

	//ビュー行列を取得
	//引数	なし
	//戻り値	ビュー行列
	XMMATRIX GetViewMatrix();

	//プロジェクション行列を取得
	//引数	なし
	//戻り値	プロジェクション行列
	XMMATRIX GetProjectionMatrix();
};