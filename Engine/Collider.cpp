#include "Collider.h"
#include "Transform.h"
#include "GameObject.h"

//コンストラクタ
Collider::Collider(XMVECTOR center, float radius) : center_(center),radius_(radius)
{
}

//デストラクタ
Collider::~Collider()
{
}

//当たっているか調べる
bool Collider::IsHit(Collider* pTarget)
{
	//相手との距離を計算する
	//コライダーがつけられたゲームオブジェクトからみたコライダーの中心位置にするために
	//ゲームオブジェクトのポジションと足し合わせる
	XMVECTOR vec = (pTarget->pGameObject_->GetPosition() + pTarget->center_) -
	 	           (pGameObject_->GetPosition() + center_);
	float length = XMVector3Length(vec).vecX;

	//当たってるか判定
	if (length <= pTarget->radius_ + radius_)
	{
		return true; //当たっている
	}

	return false; //当たっていない
}

//半径を返す
float Collider::GetRadius()
{
	return radius_; //半径
}

//ゲームオブジェクトのセット
void Collider::SetGameObject(GameObject * gameObj)
{
	pGameObject_ = gameObj;
}
