#include "Image.h"
#include <vector>

namespace Image
{
	//データ格納用
	std::vector<ImageData*> imageDatas;

	//ファイルのロード
	int Lode(std::string fileName)
	{
		//イメージデータの登録
		ImageData* pImageData = new ImageData; //作成
		pImageData->pSprite = nullptr; //初期化
		pImageData->fileName = fileName; //ファイル名登録
		//同じファイル名がすでに登録されてないか調べる
		for (int i = 0; i < imageDatas.size(); i++)
		{
			if (imageDatas[i]->fileName == fileName)
			{
			//すでにロードされている
			//ロードされているものをそのまま使用
			pImageData->pSprite = imageDatas[i]->pSprite;
			break;
			}
		}
		//まだロードされてない
		if (pImageData->pSprite == nullptr)
		{
			//新しくロードする
			pImageData->pSprite = new Sprite;
			pImageData->pSprite->Initialize(fileName);
		}
		imageDatas.push_back(pImageData); //末尾に追加
		//データを格納した場所を返す
		int dataPos = (int)imageDatas.size() - 1;
		return dataPos;
	}

	//変形
	void SetTransform(int hImage, Transform & transform)
	{
		imageDatas[hImage]->transform = transform; //モデルの変形
	}

	//描画
	void Draw(int hImage)
	{
		imageDatas[hImage]->pSprite->Draw(imageDatas[hImage]->transform);//描画
	}

	//後処理
	void Relese()
	{
		for (int i = 0; i < imageDatas.size(); i++)
		{
			SAFE_DELETE(imageDatas[i]->pSprite);
		}
		imageDatas.clear();
	}
}
