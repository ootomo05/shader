#pragma once
#include <string>
#include <vector>
#include "Transform.h"
#include "Fbx.h"

//3Dモデルを管理する
namespace Model
{
	//モデルが持つデータ
	struct ModelData
	{
		Transform transform;  //位置、拡大縮小、回転
		std::string fileName; //モデルのファイル名
		Fbx* pFbx; //モデルデータ

		//コンストラクタ
		//pFbxを必ず初期化するようにする
		ModelData() : pFbx(nullptr) {};
	};
	

	//モデルを読み込む
	//引数 fileName 読み込みたいモデルのファイル名
	//戻り値 番号
	int Load(std::string fileName);

	//モデルをどう描画するかを指定
	//引数 hModel 読み込んだモデルの番号
	//引数 transform モデルの座標
	void SetTransform(int hModel, Transform& transform);

	//モデルの描画
	//引数 hModel 描画したいモデルの番号
	void Draw(int hModel);

	//自分の子供をすべて削除
	//引数 なし
	void AllDelete();
	void RayCast(int handle, RayCastData * rayData);
}