#pragma once

#include <dInput.h>
#include <DirectXMath.h>
#include "XInput.h"
#include "Global.h"

//リンカ
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dInput8.lib")
#pragma comment(lib,"Xinput.lib")


//入力処理を管理
namespace Input
{
	//初期化
	//引数 hWnd ウィンドウハンドル
	//戻り値 成功か失敗
	HRESULT Initialize(HWND hWnd);

	//更新
	//引数 なし
	void Update();

	//引数で渡されたキーが押されているかを判定
	//引数 keyCode 押されているか調べたいキー
	//戻り値 押されているかどうか
	bool IsKey(int keyCode);

	//引数で渡されたキーが今押されてるか
	//引数 keyCode 今押されたか調べたいキー
	//戻り値 今押されたかどうか
	bool IsKeyDown(int keyCode);

	//今キーを放した
	//引数 keyCode 放したか調べたいキー
	//戻り値 今放したかどうか
	bool IsKeyUp(int keyCode);



	//マウスボタンが押されているか調べる
	//引数 buttonCode 押されたか調べたいマウスのキーコード
	//戻り値 押されたか
	bool IsMouseButton(int buttonCode);

	//マウスのボタンが今押されたか
	//引数 buttonCode 今押されたか調べたいマウスのボタンのキーコード
	//戻り値 今押されたか
	bool IsMouseButtonUp(int buttonCode);

	//マウスのボタンを今放したか調べる
	//引数 buttonCode 今放されたか調べたいマウスボタンのコード
	//戻り値 今放されたか
	bool IsMouseButtonDown(int buttonCode);

	//マウスの移動量を取得
	//引数 なし
	//戻り値 マウスの移動量
	DirectX::XMVECTOR GetMouseMove();

	//マウスポインタのウィンドウ上の座標を取得
	//引数 なし
	//戻り値 マウスポインタの座標
	DirectX::XMVECTOR GetMousePosition();

	//マウスポインタのウィンドウ上の座標を登録
	//引数 x,y マウスポインタの座標
	void SetMousePosition(int x, int y);




	//ボタンが押されているか調べる
	//引数 buttonCode 調べたいボタン
	//引数 padID 押されている調べたいコントローラー
	//戻り値 押されているかどうか
	bool IsPadButton(int buttonCode, int padID);

	//コントローラーのボタンを今放したか調べる
	//引数 buttonCode 今放したか調べたいボタン
	//引数 padID 調べたいコントローラー
	//戻り値 今放したか
	bool IsPadButtonUp(int buttonCode, int padID);

	//デッドゾーンの設定入力されても反応しない値
	//引数 raw
	//引数 max
	//引数 deadZone
	//戻り値 deadZoneを計算に含めた値
	float GetAnalogValue(int raw, int max, int deadZone);

	//左スティックの傾きを取得
	//引数 padID 調べたいコントローラー
	//戻り値 傾き具合-1~1の範囲
	DirectX::XMVECTOR GetPadStickL(int padID);

	//右スティックの傾きを取得
	//引数 padID 調べたいコントローラー
	//戻り値 傾き具合-1~1の範囲
	DirectX::XMVECTOR GetPadStickR(int padID);

	//左トリガーの押し込み具合を取得
	//引数 padID 調べたいコントローラー
	//戻り値 押し込み具合0~1の範囲
	float GetPadTrrigerL(int padID);

	//右トリガーの押し込み具合を取得
	//引数 padID 調べたいコントローラー
	//戻り値 押し込み具合0~1の範囲
	float GetPadTrrigerR(int padID);

	//バイブレーションを使う
	//モーターの強さは0~1の範囲で指定する
	//引数 motorPowerL 左側のモーターの強さ
	//引数 motorPowerR 右側のモーターの強さ
	//引数 padID 鳴らしたいコントローラー
	void PadVibration(float motorPowerL, float motorPowerR, int padID);


	//開放
	//引数 なし
	void Release();
	void SetWheel(int wheel);
	int GetWheel();
};