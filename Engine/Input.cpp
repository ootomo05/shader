#include "Input.h"
#include <algorithm>
#include <iostream>
#include <math.h>


namespace Input
{
	//Inputを使うためのクラス
	//typdefでポインタ付きの別名になっているので
	//LPDIRECINPUT8はポインタになる
	LPDIRECTINPUT8   pDInput = nullptr;

	//キーボードを使うための変数を用意
	LPDIRECTINPUTDEVICE8 pKeyDevice = nullptr; //使いたい入力装置
	BYTE keyState[256] = { 0 };		//すべてのキーが押しているかどうかを判別
	BYTE prevKeyState[256] = { 0 }; //前フレームでの各キーの状態

	//マウスを使うための変数を用意
	LPDIRECTINPUTDEVICE8 pMouseDevice = nullptr; //マウスを使用するためのオブジェクト
	DIMOUSESTATE mouseState;           //マウスの状態を取得する構造体
	DIMOUSESTATE prevMouseState;       //前フレームのマウスの状態
	DirectX::XMVECTOR mousePosition;   //マウスカーソルの位置を入れとく

	//コントローラーを使うための変数を用意
	const int MOTOR_POWER = 65535; //モーターパワーを0~1の範囲で指定するためこれをかける
	const int MAX_PAD_NUM = 4;     //接続できるコントローラーの最大数
	XINPUT_STATE controllerState[MAX_PAD_NUM];     //コントローラーの状態を記憶
	XINPUT_STATE prevControllerState[MAX_PAD_NUM]; //前フレームのコントローラーの状体を記憶
	
	//マウスのホイール量
	int mouseWheelDelta = 0;

	//初期化
	HRESULT Initialize(HWND hWnd)
	{
		//pDinput使えるように作成
		if (FAILED(DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION,
			IID_IDirectInput8, (VOID**)&pDInput, nullptr)))
		{
			MessageBox(nullptr, "Input.cpp pDinput作成失敗", "エラー", MB_OK);
			return E_FAIL;
		};

		//キーボードを使えるようにする
		if (FAILED(pDInput->CreateDevice(GUID_SysKeyboard, &pKeyDevice, nullptr)))
		{
			MessageBox(nullptr, "Input.cpp デバイスオブジェクト作成失敗", "エラー", MB_OK);
			return E_FAIL;
		};
		pKeyDevice->SetDataFormat(&c_dfDIKeyboard);
		//ほかのアプリとのキー入力の優先度を設定
		//このアプリが裏画面になった時キーを入力しても反応しないようにする
		pKeyDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);

		//マウスを使えるようにする
		if (FAILED(pDInput->CreateDevice(GUID_SysMouse, &pMouseDevice, nullptr)))
		{
			MessageBox(nullptr, "Input.cpp デバイスオブジェクト作成失敗", "エラー", MB_OK);
			return E_FAIL;
		}
		;
		pMouseDevice->SetDataFormat(&c_dfDIMouse);
		pMouseDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);

		//デバイスの作成が完了
		return S_OK;
	}

	//更新
	void Update()
	{
		//今のフレームのキーの入力状態を記憶
		memcpy(prevKeyState, keyState,sizeof(keyState));
		//キーボードを探す
		//キーが押されているかをKeyStateに入れる
		pKeyDevice->Acquire();
		pKeyDevice->GetDeviceState(sizeof(keyState), &keyState);

		//マウス
		//今の入力状態を記憶
		//今の入力状態を取得
		pMouseDevice->Acquire();
		memcpy(&prevMouseState, &mouseState, sizeof(mouseState));
		pMouseDevice->GetDeviceState(sizeof(mouseState), &mouseState);

		//コントローラーが接続でき最大数回す
		for (int i = 0; i < MAX_PAD_NUM; i++)
		{
			//コントローラー
			//今の入力状態を記憶
			//今の入力の状態を取得
			memcpy(&prevControllerState[i], &controllerState[i], sizeof(controllerState[i]));
			XInputGetState(0, &controllerState[i]);
		}
		
	}

	//キーが押されてるか
	bool IsKey(int keyCode)
	{
		//キーが押されてるかを調べる
		//keyStateのkeyCode番目の値の上位1bit目が
		//キーが押されているかのフラグなので
		//上位1bitが1か0を調べる
		//1なら押されていることになる
		bool isKey = (keyState[keyCode] & 0x80);

		//押されているならtrueが返される
		return isKey;
	}

	//今押されたか
	bool IsKeyDown(int keyCode)
	{
		//今は押してて、前回は押してない
		//今のフレームでキーが押されていて
		//前のフレームでキーが押されてないなら
		//今キーを押したことになる
		bool isKeyDown = (IsKey(keyCode) && !(prevKeyState[keyCode] & 0x80));

		//今キーが押されたか、前のフレームからキーが押されている
		return isKeyDown;
	}

	//いま放したか
	bool IsKeyUp(int keyCode)
	{
		//Iskeyの戻り値がfalseなら今のフレームでは押されてない
		//prevKeyStateがtrueなら前のフレームでは押されている
		//前フレームで押されていて、今のフレームで押されてないということは
		//今のフレームで放されたことになる
		bool isKeyUp = (!IsKey(keyCode) && (prevKeyState[keyCode] & 0x80));

		//キーが押されているか、前のフレームでキーが押されていない
		return isKeyUp;
	}

	//マウス情報取得

	//マウスのボタンが押されているか調べる
	bool IsMouseButton(int buttonCode)
	{
		bool isMouseButton = (mouseState.rgbButtons[buttonCode] & 0x80);

		return isMouseButton;
	}

	//マウスのボタンが今放されたか調べる
	bool IsMouseButtonUp(int buttonCode)
	{
		//IsMouseButtonの戻り値がfalseなら今のフレームでは押されてない
		//prevMouseStateがtrueなら前のフレームでは押されている
		//前フレームで押されていて、今のフレームで押されてないということは
		//今のフレームで放されたことになる
		bool isMouseButtonUp = (!(IsMouseButton(buttonCode)) && (prevMouseState.rgbButtons[buttonCode] & 0x80));

		return isMouseButtonUp;
	}

	//マウスのボタンが今押された調べる
	bool IsMouseButtonDown(int buttonCode)
	{
		//今は押してて、前回は押してない
		//今のフレームでキーが押されていて
		//前のフレームでキーが押されてないなら
		//今キーを押したことになる
		bool isMouseButtonDown = ((IsMouseButton(buttonCode)) && !(prevMouseState.rgbButtons[buttonCode] & 0x80));

		return isMouseButtonDown;
	}

	//そのフレームでのマウスの移動量を取得
	DirectX::XMVECTOR GetMouseMove()
	{
		DirectX::XMVECTOR result = DirectX::XMVectorSet((float)mouseState.lX, (float)mouseState.lY, (float)mouseState.lZ, 0);
		return result;
	}

	//現在のマウスの座標を取得
	DirectX::XMVECTOR GetMousePosition()
	{
		return mousePosition;
	}

	//マウスの現在の位置をセット
	void SetMousePosition(int x, int y)
	{


		mousePosition = DirectX::XMVectorSet((float)x, (float)y, 0, 0);
	}


	//コントローラー

	//コントローラーのボタンが押されたか調べる
	bool IsPadButton(int buttonCode,int padID)
	{
		//ボタンが押されているか
		if (controllerState[padID].Gamepad.wButtons & buttonCode)
		{
			return true; //押されている
		}
		return false; //押されていない
	}

	//コントローラーのボタンを今押したか調べる
	bool IsPadButtonDown(int buttonCode, int padID)
	{
		//今は押してて、前回は押してない
		//今のフレームでキーが押されていて
		//前のフレームでキーが押されてないなら
		//今キーを押したことになる
		if (IsPadButton(buttonCode, padID) && !(prevControllerState[padID].Gamepad.wButtons & buttonCode))
		{
			return true; //今押した
		}
		return false;
	}

	//コントローラーのボタンを今放したか調べる
	bool IsPadButtonUp(int buttonCode, int padID)
	{
		//IsPadButtonの戻り値がfalseなら今のフレームでは押されてない
		//prevControllerState[padID]がtrueなら前のフレームでは押されている
		//前フレームで押されていて、今のフレームで押されてないということは
		//今のフレームで放されたことになる
		if (!IsPadButton(buttonCode, padID) && prevControllerState[padID].Gamepad.wButtons & buttonCode)
		{
			return true; //今放した
		}
		return false;
	}

	//デッドゾーンの設定
	float GetAnalogValue(int raw, int max, int deadZone)
	{
		//デッドゾーン以内に入っていないならそのまま戻り値として返す
		float result = (float)raw;


		if (result > 0)
		{
			//デッドゾーン
			if (result < deadZone)
			{
				result = 0;
			}
			else
			{
				result = (result - deadZone) / (max - deadZone);
			}
		}

		else
		{
			//デッドゾーン
			if (result > -deadZone)
			{
				result = 0;
			}
			else
			{
				result = (result + deadZone) / (max - deadZone);
			}
		}

		return result;
	}


	//左スティックの傾きを取得
	DirectX::XMVECTOR GetPadStickL(int padID)
	{
		//デッドゾーンを考慮した傾きを返す
		float x = GetAnalogValue(controllerState[padID].Gamepad.sThumbLX, 32767, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
		float y = GetAnalogValue(controllerState[padID].Gamepad.sThumbLY, 32767, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
		return DirectX::XMVectorSet(x, y, 0, 0);
	}

	//右スティックの傾きを取得
	DirectX::XMVECTOR GetPadStickR(int padID)
	{
		//デッドゾーンを考慮した傾きを返す
		float x = GetAnalogValue(controllerState[padID].Gamepad.sThumbRX, 32767, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
		float y = GetAnalogValue(controllerState[padID].Gamepad.sThumbRY, 32767, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
		return DirectX::XMVectorSet(x, y, 0, 0);
	}

	//左トリガーの押し込み具合を取得
	float GetPadTrrigerL(int padID)
	{
		//デッドゾーンを考慮した押し込み具合を返す
		return GetAnalogValue(controllerState[padID].Gamepad.bLeftTrigger, 255, XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
	}

	//右トリガーの押し込み具合を取得
	float GetPadTrrigerR(int padID)
	{
		//デッドゾーンを考慮した押し込み具合を返す
		return GetAnalogValue(controllerState[padID].Gamepad.bRightTrigger, 255, XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
	}

	//バイブレーション機能を使う
	void PadVibration(float motorPowerL,float motorPowerR,int padID)
	{
		

		//コントローラーのバイブ機能を使う
		//バイブの初期化
		//コントローラー左と右のバイブの強さを設定
		XINPUT_VIBRATION vibration;
		ZeroMemory(&vibration, sizeof(XINPUT_VIBRATION));
		vibration.wLeftMotorSpeed  = WORD(motorPowerL * MOTOR_POWER); // 左モーターの強さ
		vibration.wRightMotorSpeed = WORD(motorPowerR * MOTOR_POWER); // 右モーターの強さ
		XInputSetState(padID, &vibration); //どのコントローラーのバイブを鳴らすかを指定
	}
	
	//開放
	void Release()
	{
		SAFE_RELEASE(pDInput);
		SAFE_RELEASE(pKeyDevice);
		SAFE_RELEASE(pMouseDevice);
	}

	void SetWheel(int wheel)
	{
		mouseWheelDelta = wheel;
	}

	int GetWheel()
	{
		return mouseWheelDelta;
	}
}