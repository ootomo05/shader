#include "SceneManager.h"
#include "../TestScene.h"
#include "Model.h"
#include "Image.h"

SceneManager::SceneManager(GameObject * parent)
	: GameObject(parent, "SceneManager"),nextSceneID_(SCENE_ID_TEST)
{
}

//初期化
void SceneManager::Initialize()
{
	Instantiate<TestScene>(this);
	nowSceneID_ = SCENE_ID_TEST;
}

//更新
void SceneManager::Update()
{
	//シーンを切り替える処理
	if (nowSceneID_ != nextSceneID_)
	{
		//新しいシーンにするために
		//片づける処理
		DeleteAllChildren(); //現在の子供以下をすべて削除

		Model::AllDelete(); //現在のシーンで使っている3Dモデルを開放
		Image::Relese();    //現在のシーンで使っている画像を開放
		
		//次のシーンを作成する
		switch (nextSceneID_)
		{
		case SCENE_ID_TEST:Instantiate<TestScene>(this); break;
		}

		nowSceneID_ = nextSceneID_; //現在のシーンを更新
	}
}

//描画
void SceneManager::Draw()
{
}

//開放
void SceneManager::Release()
{
}

//シーンを切り替える
void SceneManager::ChangeScene(SCENE_ID next)
{
	nextSceneID_ = next;
}
